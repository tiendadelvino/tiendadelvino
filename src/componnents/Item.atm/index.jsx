import React from 'react';


// eslint-disable-next-line react/prop-types
const Item = ({ dataIndex, viewport }) => (
  <>
    {viewport ? (
      <img
        src={require(`../../assets/images/vtn${dataIndex}.png`)}
         alt="text"
      />
    ) : (
      <img
        src={require(`../../assets/images/vtn${dataIndex}-mobile.png`)}
        alt="text"
      />
    )}

  </>
);


export default Item;
