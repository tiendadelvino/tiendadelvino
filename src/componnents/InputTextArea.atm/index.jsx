import React from 'react';
import Proptypes from 'prop-types';
import styles from './InputTextArea.module.scss';

const InputTextArea = ({ textLabel, inputName }) => (

  <label htmlFor={inputName} className={`${styles['input-text']} `}>
    <textarea
      rows="3"
      name={inputName}
      id={inputName}
      placeholder="&nbsp;"
      required
    />
    <span className={`${styles['input-text--label']} `}>{textLabel}</span>
    <span className={`${styles['input-text--border']} `} />
  </label>

);

InputTextArea.propTypes = {
  textLabel: Proptypes.string,
  inputName: Proptypes.string,
};

InputTextArea.defaultProps = {
  textLabel: 'input text default',
  inputName: 'default',
};

export default InputTextArea;
