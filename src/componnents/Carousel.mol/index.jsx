import React, { Component } from 'react';
// eslint-disable-next-line camelcase
import {chevronLeft} from 'react-icons-kit/fa/chevronLeft';
import {chevronRight} from 'react-icons-kit/fa/chevronRight';
import Icon from 'react-icons-kit';
import Item from '../Item.atm';
import carouselStyle from './_carousel.module.scss'


class Carousel extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: this.props.data.items,
      currentIndex: 0,
      translateValue: 0,
      scrollPos:0
    }
  }


  goToPrevSlide = () => {
    if(this.state.currentIndex >= this.state.items.length)
      return;
     this.setState((prevState) => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: prevState.translateValue + this.cardWidth()
    }))
    if(this.state.currentIndex === 0) {
      this.setState(prevState => ({
        currentIndex: 0,
        translateValue: 0
      }));
    }
  }

  goToNextSlide = () => {
    if(this.state.currentIndex === (this.state.items.length) -1 )
    return;
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue  + (-this.cardWidth())
    }));

    if(this.state.translateValue === ((this.state.items.length-1)*this.cardWidth())) {
      this.setState(prevState => ({
        currentIndex: this.state.items.length
      }));
    }
  }
  
  cardWidth = () => {
     return document.querySelector('.item').clientWidth
  }

  carouselWidth= () => {
    return document.querySelector(".ltdv-carousel").offsetWidth
}


  render() {
    console.log(this.state.items)
    return (
   
      <div data-testid="carousel-ltdv" 
      className= {`${carouselStyle[`carousel`]}  ltdv-carousel`}  onTouchEndCapture ={this.handleTouchMove}  >

        <div className= {`${carouselStyle[`back-arrow`]}`} onClick={ this.goToPrevSlide}>
        <div className= {`${carouselStyle[`arrow-icon`]}` }>
          <Icon size={32} icon={chevronLeft} />
        </div>
        </div>

        <div className= {`${carouselStyle[`next-arrow`]}` } onClick={this.goToNextSlide}>
        <div className= {`${carouselStyle[`arrow-icon`]}` } >
          <Icon size={32} icon={chevronRight} />
        </div>
        </div>
     
        <div className= {`${carouselStyle[`carousel--items`]} `}  style={{
            transform: `translateX(${this.state.translateValue}px)`,
            transition: 'transform ease-out 0.45s'
          }}> 
          
          {
          this.props.data.items.map((data, index) =>
            <div className= {`${carouselStyle[`item`]} item `} key={index}>
             <Item dataIndex={data.itemId} viewport={this.props.viewport} /> 
            </div>
          )
        }
        </div>
      </div>
    )
  }
}
export default Carousel; 
