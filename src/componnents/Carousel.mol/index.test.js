import React from 'react';
import ReactDOM from 'react-dom';
import Carousel from './index';

// eslint-disable-next-line no-undef
it('Carousel', ({ Items, isDesktop }) => {
  const div = document.createElement('div');
  // eslint-disable-next-line react/jsx-filename-extension
  ReactDOM.render(<Carousel data={Items} viewport={isDesktop} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
