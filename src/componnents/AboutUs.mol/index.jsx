import React from 'react';
import Title from '../Title.atm';
import styles from './AboutUs.module.scss';

// eslint-disable-next-line react/prop-types
const AboutUs = () => (
  <div className={`${styles['wrapper-about']} `}>
    <div data-testid="aboutus-title" className={`${styles['wrapper-about__title']} `}>
      <Title family="anton" color="light" text="NOSOTROS" fontSize="omega" />
    </div>
    <div className={`${styles['wrapper-about__image']} `}>
      <img
        src={require(`../../assets/images/imgAbout.jpg`)}
        alt="text"
      />
    </div>

    <div className={`${styles['wrapper-about__text']}`}>
      Por mucho tiempo buscamos como plasmar y dar a conocer nuestro 
      amor por probar y degustar los mejores vinos de autor del país. 
      Vinos de producción pequeñas, elaborados a mano y donde prácticamente 
      no hay intervención de máquinas. Ahí nace nuestro proyecto llamado la 
      tienda del vino. Nuestro foco es buscar, probar y darles a conocer los mejores 
      vinos de autor del país, donde pequeños productores vitivinícolas con mucho 
      esfuerzo amor y pasión elaboran vinos de calidad inigualable, todo esto para 
      llevárselos hasta ustedes a través nuestra tienda online latiendadelvino.cl.
    </div>
  </div>
);


export default AboutUs;
