import React from 'react';
import Proptypes from 'prop-types';
import Style from './Title.module.scss';

const Title = (
  {
    family, color, text, weight, align, fontSize,
  },
) => (
  <>
    <div
      data-testid="title"
      className={`${Style[`title--${weight}`]} 
      ${Style[`title--${align}`]} 
      ${Style[`title--${color}`]}
      ${Style[`title--${family}`]} 
      ${Style[`title--font-${fontSize}`]}`}
    >
      { text }
    </div>

  </>
);

Title.propTypes = {
  text: Proptypes.string,
  family: Proptypes.oneOf(['anton', 'monserrat']),
  color: Proptypes.oneOf(['dark', 'light']),
  fontSize: Proptypes.oneOf(['caption', 'kappa', 'omega', 'zeta', 'epsilon', 'delta', 'gamma', 'beta', 'alpha', 'mega', 'giga']),
  weight: Proptypes.oneOf(['bold', 'normal']),
  align: Proptypes.oneOf(['center', 'left', 'right']),

};

Title.defaultProps = {
  color: 'dark',
  family: 'anton',
  text: 'Componente de título',
  fontSize: 'kappa',
  weight: 'normal',
  align: 'left',
};

export default Title;
