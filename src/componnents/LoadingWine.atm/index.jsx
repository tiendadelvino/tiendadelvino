import React from 'react';
import Proptypes from 'prop-types';
import styles from './LoadingWine.module.scss';

const LoadingWine = ({
  testId,
}) => (
  <div data-testid={testId}  className={`${styles[`loading-wine`]} `} >
    <img
        src={require(`../../assets/images/wine-loading.gif`)}
        alt="text"
    />
   <span className={`${styles[`loading-wine__text`]} `}>Cargando...</span> 
  </div>
  
);

LoadingWine.propTypes = {
  testId: Proptypes.string,
};

LoadingWine.defaultProps = {
  testId: 'loading-wine',
};

export default LoadingWine;
