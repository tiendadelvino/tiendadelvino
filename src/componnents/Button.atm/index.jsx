import React from 'react';
import Proptypes from 'prop-types';
import './Button.scss';
import styles from './Button.module.scss';

const defaultFunc = () => {
  // eslint-disable-next-line no-console
  console.log('default function button');
};

const Button = ({
  testId, textButton, action, type,
}) => (
  <div data-testid="Button">
    <button className={`${styles[`mybutton--${type}`]} `} type="button" onClick={action}>
      { textButton }
    </button>
  </div>
);

Button.propTypes = {
  testId: Proptypes.string,
  textButton: Proptypes.string,
  action: Proptypes.func,
  type: Proptypes.oneOf(['pill', 'pill-outline', 'pill-disable', 'squere', 'circle', 'send', 'addToCard']),
};

Button.defaultProps = {
  testId: 'Button',
  textButton: 'Boton',
  action: defaultFunc,
  type: 'pill',
};

export default Button;
