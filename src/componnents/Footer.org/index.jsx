import React from 'react';
// eslint-disable-next-line camelcase
import { instagram } from 'react-icons-kit/fa/instagram';
import { facebookOfficial } from 'react-icons-kit/fa/facebookOfficial';
import Icon from 'react-icons-kit';
import Title from '../Title.atm';
import Contact from '../Contact.mol';
import Notification from '../Notification.atm';
import Logo from '../Logo.atm';
import Style from './Footer.module.scss';

const Footer = () => (
  <div id="footer" data-testid="footer" className={`${Style['footer-wraper']} `}>
    <div className={`${Style['footer-wraper__row']} `}>
      <div data-testid="wrapper-footer-title" className={`${Style['footer-wraper__title']} `}>
        <Title family="anton" color="light" text="CONTÁCTANOS" fontSize="omega" />
      </div>
      <div className={`${Style['footer-wraper--col6']} `}>
        <Contact />
        {/* <div className={`${Style['footer-wraper--col3']} `}><Logo /></div> */}
        <div className={`${Style['footer-wraper--col9']} `}>
          <div className={`${Style['footer-wraper--social']}`}>
            <span>
              <a href="https://www.instagram.com/tiendadelvino/" rel="noopener noreferrer" aria-label="Read more about latiendadelvino in instagram" target="_blank">
                <Icon size={32} icon={instagram} />
              </a>
            </span>
            <span>
              <a href="https://www.facebook.com/latiendadelvino/" rel="noopener noreferrer" aria-label="Read more about latiendadelvino in facebook" target="_blank">
                <Icon size={32} icon={facebookOfficial} />
              </a>
            </span>
          </div>

        </div>
      </div>
      <div className={`${Style['footer-wraper--col6']} `}>
        {/* <Notification /> */}
      </div>
    </div>
    <div className={`${Style['footer-wraper__last-row']} `}>
        La tienda del vino © Copyright 2019
    </div>
  </div>
);

export default Footer;
