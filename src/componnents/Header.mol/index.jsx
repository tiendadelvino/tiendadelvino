import React from 'react';
import Nav from '../Nav.mol';
import Logo from '../Logo.atm'
import Style from './Header.module.scss';


const Header = () => (
  <>
    <div data-testid="header" className={`${Style.header} `}>
      <div className={`${Style.header__wrapper} `}>
        <div className={`${Style['header--col3']} `}>
          <Logo logoSize="xs" logoPosition="center" logoId="top-header" />
        </div>
        <div className={`${Style['header--col9']} `}>
          <Nav />
        </div>
      </div>
    </div>

  </>
);

export default Header;
