import React from 'react';
import Proptypes from 'prop-types';
import styles from './SendButton.module.scss';

const SendButton = ({
  testId, textButton, type,
}) => (
  <>
    <button data-testid={testId} name={testId} className={`${styles[`mybutton--${type}`]} `} type="submit" value={textButton}>
      { textButton }
    </button>
  </>
);

SendButton.propTypes = {
  testId: Proptypes.string,
  textButton: Proptypes.string,
  type: Proptypes.oneOf(['pill', 'pill-outline', 'pill-disable', 'squere', 'circle', 'send', 'addToCard']),
};

SendButton.defaultProps = {
  testId: 'submit',
  textButton: 'Enviar',
  type: 'send',
};

export default SendButton;
