/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import React, { Component } from 'react';
import Style from './Nav.module.scss';


class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // eslint-disable-next-line react/no-unused-state
      mobileDisplay: false,
    };
  }


  render() {
    return (

      <div className={`${Style.nav} `}>
        <ul>
          <li><a alt="1" href="#header">Nosotros</a></li>
          <li><a alt="2" href="#content">La Tienda</a></li>
          <li><a alt="4" href="#footer">Contacto</a></li>
          <li><a href="https://www.tiendadelvino.cl/" target="_blank" rel="external">Comprar</a></li>
        </ul>
      </div>
    );
  }
}
export default Nav;
