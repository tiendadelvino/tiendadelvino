import React from 'react';
import Proptypes from 'prop-types';
import styles from './InputText.module.scss';



const InputText = ({ textLabel, inputName }) => (

  <label htmlFor={inputName} className={`${styles['input-text']} `}>
    <input
      type="text"
      name={inputName}
      id={inputName}
      placeholder="&nbsp;"
      required
    />
    <span className={`${styles['input-text--label']} `}>{textLabel}</span>
    <span className={`${styles['input-text--border']} `} />
  </label>

);

InputText.propTypes = {
  textLabel: Proptypes.string,
  inputName: Proptypes.string,
};

InputText.defaultProps = {
  textLabel: 'input text default',
  inputName: 'default',
};

export default InputText;
