import React from 'react';

const Wip = () => (
  <div>
    <h1>
      Estamos trabajando en nuestro nuevo sito para ofrecerles las mejores cepas
      y vinos de autor. Atentos los próximos días.
    </h1>
    <h4>Email: contacto@latiendadelvino.cl</h4>
    <h4>
      Instagram:
      <a target="_blank" href="https://www.instagram.com/tiendadelvino/" rel="noreferrer noopener">
        @tiendadelvino
      </a>
    </h4>
    <h4>
      Facebook:
      <a target="_blank" href="https://www.facebook.com/latiendadelvino/" rel="noreferrer noopener">
        @latiendadelvino
      </a>
    </h4>
  </div>
);

export default Wip;
