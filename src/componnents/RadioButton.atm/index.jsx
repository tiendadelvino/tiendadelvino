import React from 'react';
import Proptypes from 'prop-types';
import Styles from './RadioButton.module.scss';


const RadioButton = ({ Name, TextOption, Option, Id, Disabled }) => (
  <div className={`${Styles.radio} `}>
    <label htmlFor={Id}>
      {Disabled === true ? <input type="radio" value={Option} name={Name} id={Id} disabled />
        : <input type="radio" value={Option} name={Name} id={Id} />}
      <span>{ TextOption }</span>
    </label>

  </div>
);

RadioButton.propTypes = {
  Name: Proptypes.string,
  TextOption: Proptypes.string,
  Option: Proptypes.string,
  Id: Proptypes.string,
  // eslint-disable-next-line react/require-default-props
  Disabled: Proptypes.bool,

};
RadioButton.defaultProps = {
  Name: 'RadioButton',
  TextOption: 'RadioButton Component',
  Option: 'RadioButton',
  Id: 'RadioButton',
  // eslint-disable-next-line react/default-props-match-prop-types
  Disable: false,
};

export default RadioButton;
