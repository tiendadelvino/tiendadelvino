import React from 'react';
import Proptypes from 'prop-types';
import LogoImg from '../../assets/images/logo1.png';
// eslint-disable-next-line import/no-unresolved
import Style from './Logo.module.scss';

const Logo = (
  {
    logoSize, logoPosition, logoId,
  },
) => (
  <>
    <div
      data-testid={logoId}
      className={`${Style[`logo--${logoSize}`]} 
      ${Style[`logo--${logoPosition}`]} 
      `}
    >
      <img src={LogoImg} alt="logo latiendadelvino" />
    </div>

  </>
);

Logo.propTypes = {
  logoSize: Proptypes.oneOf(['xs', 'sm', 'md', 'full']),
  logoPosition: Proptypes.oneOf(['left', 'center', 'right']),
  logoId: Proptypes.string,

};

Logo.defaultProps = {
  logoSize: 'full',
  logoPosition: 'left',
  logoId: 'default',
};

export default Logo;
