import React from 'react';
// eslint-disable-next-line import/no-unresolved
import Title from '../Title.atm';
// eslint-disable-next-line import/no-named-as-default
import Product from '../Product.atm';
import Notification from '../Notification.atm';
// import dataProducts from '../../services/products.json';
import Style from './Store.module.scss';

// eslint-disable-next-line react/prop-types
const Store = ({ hits }) => (
  <div data-testid="content" className={`${Style['store-wraper']} `}>
    <div data-testid="store-footer-title" className={`${Style['store-wraper__title']} `}>
      <Title family="anton" color="light" text="LA TIENDA" fontSize="omega" />
    </div>
     {/*<Notification />*/}
    {
      // eslint-disable-next-line react/no-array-index-key
      hits.map((item, index) => <Product key={index} productInfo={item} />)
    }

  </div>
);
export default Store;
