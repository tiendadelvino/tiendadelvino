/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import React, { Component } from 'react';
import Notification from '../Notification.atm';
import Style from './Product.module.scss';


class Product extends Component {

  constructor(props) {
    super(props);
    this.state = {
      moreInfo: false
    };
  }

  moreDetails =()=> {
		this.setState({
			moreInfo: true
	  });
    /*const modal = document.querySelector("."+Style['more-info']);
    modal.classList.remove("disapear");
    modal.classList.add("slide--in-bottom");
    const modal = document.querySelector(".modalinfo");
    Array.from(modal).forEach(function(element) {
      element.addEventListener('click', myFunction);
    });*/
  
  };
  
  closeDetails =()=> {
		this.setState({
			moreInfo: false
		});
	
	};

 

  render() {

    return (
     <div className={`${Style['wrapper-product']}`}>
       <div className={`${Style.product} `}>
      <div className={`${Style.product__info} `}>
        <div className={`${Style['product__info--dateEdition']}`}>
        Edición:
          <span> {this.props.productInfo.dateEdition}</span>
        </div>
        <div className={`${Style['product__info--brand']}`}>
          colección: {this.props.productInfo.brand}
        </div>
  
        <div className={`${Style['product__info--name']} `}>
        {this.props.productInfo.name}
        </div>
        
        {/*<div className={`${Style['product__info--price']}`}>
        <span className={`${Style['product__info--price-signe']}`}>$</span>
          {this.props.productInfo.price}
        </div>
      */}
      </div>
      <div className={`${Style.product__image} `}>
        
        <img
          src={`https://www.latiendadelvino.cl/images/products/${this.props.productInfo.id}.png`}
          alt="text"
        />
      </div>
      <div className={`${Style['product__info--type-wrapper']} `}>
          <span className={`${Style['product__info--type-wine']} `}>
          {this.props.productInfo.typeOfWine==="Tinto" ? 
            <img
              src={require(`../../assets/images/redWine.png`)}
              alt="text"
            />: <img
            src={require(`../../assets/images/whiteWine.png`)}
            alt="text"
          /> }
           <p> {this.props.productInfo.typeOfWine}</p>
          </span>
          <span className={`${Style['product__info--type-cepa']} `}>
            
            <img
              src={require(`../../assets/images/cepa.png`)}
              alt="text"
            />
            <p>{this.props.productInfo.typeOfCepa}</p>
          </span>
        </div>
      <div className={`${Style['product__more-details']}`}  onClick={this.moreDetails}>
        <span className={`${Style['product__more-details--plus']}`}>+</span>
      </div>
      
    </div>
    {this.state.moreInfo ?  
    <>
    <div className={`${Style['more-info']}`} >
      <div className={`${Style['more-info__wrapper']}`}>
      
      <div className={`${Style['more-info__wrapper__info']}`}>
      <div className={`${Style['more-info__wrapper__info--name']} `}>
        {this.props.productInfo.name}
        </div>
        <div className={`${Style['more-info__wrapper__info--dateEdition']}`}>
        Edición:
          <span> {this.props.productInfo.dateEdition}</span>
        </div>
        <div className={`${Style['more-info__wrapper__info--brand']}`}>
          colección: {this.props.productInfo.brand}
        </div>
        {/* <div className={`${Style['more-info__wrapper__info--price']}`}>
        <span className={`${Style['more-info__wrapper__info--price-signe']}`}>$</span>
          {this.props.productInfo.price}
        </div> */}

        <div className={`${Style['more-info__wrapper__info--description']} `}>
        {this.props.productInfo.description}
        </div>
        
        <div className={`${Style['more-info__wrapper__info--type-wrapper']} `}>
          <span className={`${Style['more-info__wrapper__info--type-wine']} `}>
          {this.props.productInfo.typeOfWine==="Tinto" ? 
            <img
              src={require(`../../assets/images/redWine.png`)}
              alt="text"
            />: <img
            src={require(`../../assets/images/whiteWine.png`)}
            alt="text"
          /> }
            {this.props.productInfo.typeOfWine}
          </span>
          <span className={`${Style['more-info__wrapper__info--type-cepa']} `}>
            
            <img
              src={require(`../../assets/images/cepa.png`)}
              alt="text"
            />
            {this.props.productInfo.typeOfCepa}
          </span>
        </div>
       
      </div>
      <div className={`${Style['more-info__wrapper__image']}`}>
      
        <img
          src={`https://www.latiendadelvino.cl/images/products/${this.props.productInfo.id}.png`}
          alt="text"
        />
      </div>
      <div style={{
        position: 'relative',
    bottom: '25px',
    zIndex: '9999',
    left: '-10px',}}>
      {/*  <Notification />*/}
    </div>
     
    </div>
    </div>
    <div className={`${Style['bkg-more-info']}`} onClick={this.closeDetails}></div>
    </>
    : <></>}



     </div>
    )
  }
}
export default Product;
