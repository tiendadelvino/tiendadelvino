import React from 'react';
import Proptypes from 'prop-types';
import Style from './Notification.module.scss';

const Notification = (
  {
    type, notificationId,
  },
) => (
  <>
    <div
      data-testid={notificationId}
      className={`${Style['not-sale']}`}
    >
      <span className={`${Style['not-sale__text']}`}>
      Puedes pagar con Red Compra o Transferencia Electronica.
        <strong> Haga sus pedidos al whatsapp +56959150067</strong>
      </span>
      <span className={`${Style['not-sale__image']}`}>
        <img
          // eslint-disable-next-line global-require
          src={require('../../assets/images/transbank.png')}
          alt="transbank"
        />
      </span>

    </div>

  </>
);

Notification.propTypes = {
  type: Proptypes.oneOf(['left', 'center', 'right']),
  notificationId: Proptypes.string,

};

Notification.defaultProps = {
  type: 'left',
  notificationId: 'default',
};

export default Notification;
