import React, { Component } from 'react';
import InputText from '../InputText.atm';
import InputTextArea from '../InputTextArea.atm';
import SendButton from '../SendButton.atm';
import Style from './Contact.module.scss';


// eslint-disable-next-line react/prefer-stateless-function
export default class Contact extends Component {
  render() {
    return (

      <div data-testid="contact" className={`${Style['contact-wraper']} `}>
        <div className={`${Style['contact-wraper__row']} `}>

          <form id="contact-form" action="https://latiendadelvino.cl/contacto.php" method="post">
            <div className={`${Style['contact-wraper__input-row']} `}>
              <InputText textLabel="Nombre y Apellido" inputName="nombre" />
            </div>
            <div className={`${Style['contact-wraper__input-row']} `}>
              <InputText textLabel="ejemplo@correo.com" inputName="email" />
            </div>
            <div className={`${Style['contact-wraper__input-row']} `}>
              <InputText textLabel="Título del mensaje" inputName="subjet" />
            </div>

            <div className={`${Style['contact-wraper__input-row']} `}>
              <InputTextArea textLabel="Mensaje" inputName="mensaje" />
            </div>

            <SendButton testId="submit" textButton="Enviar" type="send" />

          </form>

        </div>
      </div>

    );
  }
}
