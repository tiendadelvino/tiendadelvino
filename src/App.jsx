/* eslint-disable import/no-named-as-default */
import React, { Component } from 'react';
import Footer from './componnents/Footer.org';
import Header from './componnents/Header.mol';
// eslint-disable-next-line import/no-named-as-default-member
import Carousel from './componnents/Carousel.mol';
import Store from './componnents/Store.mol';
import AboutUs from './componnents/AboutUs.mol';
import LoadingWine from './componnents/LoadingWine.atm';
// import './App.scss';
// eslint-disable-next-line import/no-unresolved
import Styles from './App.module.scss';
import Items from './services/carruselData.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hits: null,
      loading: false,
      isDesktop: false,

    };
    this.updatePredicate = this.updatePredicate.bind(this);
  }


  async componentDidMount() {
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
    const proxyurl = 'https://cors-anywhere.herokuapp.com/';
    const url = 'https://www.latiendadelvino.cl/products.json';
    const response = await fetch(url, {
    // const response = await fetch(url, {
      method: 'GET',
      dataType: 'json',
      // this headers section is necessary for CORS-anywhere
      headers: {
        'x-requested-with': 'xhr',
      },
    });
    const data = await response.json();
    this.setState({ hits: data, loading: false });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePredicate);
  }

  updatePredicate() {
    this.setState({ isDesktop: window.innerWidth > 1000 });
  }

  render() {
    const { loading, hits, isDesktop } = this.state;
    return (
      <>
        { loading || !hits ? (<LoadingWine />)
          : (
            <div>
              <div id="header">
                <Header />
              </div>
              <div className={`${Styles.vitrina} `}>

                <Carousel data={Items} viewport={isDesktop} />

              </div>
              <div id="content" className={`${Styles['main-wrapper-app']} `}>
                <AboutUs />
                <div id="store" className={`${Styles['main-wrapper-app--row']} `}>

                  <Store hits={hits} />
                </div>
              </div>
              <Footer />
            </div>
          )}
      </>
    );
  }
}
export default App;
